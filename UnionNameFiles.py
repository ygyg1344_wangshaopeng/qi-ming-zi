# -*- coding:utf-8 -*-
import json
import os

class UnionNameFiles:

    @staticmethod
    def unionFile():
        # 获取文件列表
        fileList = UnionNameFiles.fileList()
        writter = open("name/name-union.txt", 'w', encoding='utf-8')
        # 解析文件，提取文字，set去重
        for f in fileList:
            with open(f, 'r', encoding='utf-8') as nameFile:
                lines = nameFile.readlines()
                for line in lines:
                    writter.write(line[0] + ',' + line[1] + ',' + line[2] + "\n")
        writter.close()


    @staticmethod
    def fileList():
        # 列出文件夹下所有的目录与文件
        fileDir = "./name/"
        fileList = list()
        for fileName in os.listdir(fileDir):
            filePath = fileDir + fileName
            if os.path.isfile(filePath):
                fileList.append(filePath)
        print("============================= 1、fileList: ", fileList)
        return fileList


if __name__ == '__main__':
    UnionNameFiles.unionFile()
    # s = "王一X"
    # print(s[0] + ',' + s[1] + ',' + s[2])