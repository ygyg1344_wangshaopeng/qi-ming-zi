# 起名字

从百度爬取推荐名字。

1、./poetry/目录下，存放唐诗、宋词、诗经等

2、执行./PoetryToWord.py，把诗词拆解成单个文字，存放在./word/words.txt文件内

3、在words.txt中挑选自己喜欢的关键字，单独存一个文件，比如words2.txt，每行一个字

4、修改应用配置文件config.ini（不解释）
```ini
# 系统配置
[system]
# 姓氏
surname = 王
# 关键字（中间字）文件
wdFile = ./word/words3.txt
# 是否打印日志 True/False
doLog = True
```

5、执行./main.py，爬取百度智能推荐的名字，存放在./name/目录下，例如查询【王雨q】,就会爬取到
- 王雨琪
- 王雨晴
- 王雨青

等等。

6、执行./UnionNameFiles.py，把多个名字文件合并成一个。

7、如果你会数据库，例如mysql，可以把名字放到数据库里(txt转csv)，做分析，例如数据库表结构
```sql
CREATE TABLE `t_name` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `del_flag` tinyint(2) NOT NULL DEFAULT '0' COMMENT '第N次删除',
  `n1` varchar(8) DEFAULT NULL COMMENT '姓',
  `n2` varchar(8) DEFAULT NULL COMMENT '第一字',
  `n3` varchar(8) DEFAULT NULL COMMENT '第二字',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7970 DEFAULT CHARSET=utf8mb4;
```
数据库表数据：
```sql
n1,n2,n3
王,一,丁
王,一,个
王,二,g
王,二,狗
王,雨,涵
王,雨,甜
王,雨,菲
王,雨,琪
王,雨,晴
王,雨,青
```
清理、 筛选数据
```sql
-- 清理无效名字
UPDATE t_name SET del_flag = 1 WHERE n3 in ('a', 'b', 'c' ,...);

-- 清理坏字数据
UPDATE t_name SET del_flag = 2 WHERE n3 in ('狗', '蛋', '娘' ,...);

-- 清理敏感数据，例如长辈字号，国、华、忠等需要排除的
UPDATE t_name SET del_flag = 3 WHERE n2 in ('国', '华', '忠' ,...);

-- 剩下的就是可以做参考的了
SELECT
	id,
	CONCAT( n1, n2, n3 ) AS full_name 
FROM
	t_name 
WHERE
	del_flag = 0
```
